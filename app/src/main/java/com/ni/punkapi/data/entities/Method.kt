package com.ni.punkapi.data.entities

data class Method(
    var mashTemp : List<MashTemp>,
    var fermentation : Fermentation,
    var twist : String
)