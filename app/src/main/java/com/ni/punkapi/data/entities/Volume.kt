package com.ni.punkapi.data.entities

data class Volume(
    var value : Int,
    var unit : String
)