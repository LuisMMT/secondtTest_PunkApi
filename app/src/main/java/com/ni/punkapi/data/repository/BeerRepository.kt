package com.ni.punkapi.data.repository

import com.ni.punkapi.data.entities.Beer
import com.ni.punkapi.data.local.BeerDao
import com.ni.punkapi.data.remote.BeerRemoteDataSource
import com.ni.punkapi.utils.performGetOperation
import com.ni.punkapi.utils.performGetOperationLocal
import javax.inject.Inject

class BeerRepository @Inject constructor(
    private val remoteDataSource: BeerRemoteDataSource,
    private val localDataSource: BeerDao
){

    fun getBeer(id: Int) = performGetOperationLocal(
        databaseQuery = { localDataSource.getBeer(id) }
    )



    fun getBeers() = performGetOperation(
        databaseQuery = { localDataSource.getAllBeers() },
        networkCall = { remoteDataSource.getBeers()},
        saveCallResult = { localDataSource.insertAll(it) }
    )
}
