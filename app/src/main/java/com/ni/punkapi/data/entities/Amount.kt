package com.ni.punkapi.data.entities

data class Amount(
    var value : Int,
    var unit : String

)