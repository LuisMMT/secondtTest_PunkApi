package com.ni.punkapi.data.entities

data class MashTemp (
    var temp : Temp,
    var duration : Int
)
