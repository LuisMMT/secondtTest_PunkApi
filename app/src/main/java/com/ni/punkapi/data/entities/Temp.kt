package com.ni.punkapi.data.entities

data class Temp(
        var value : Int,
        var unit : String
)