package com.ni.punkapi.data.entities

import java.io.Serializable

data class BeerList(
    val results: List<Beer>
)