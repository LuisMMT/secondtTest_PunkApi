package com.ni.punkapi.data.entities

data class BoilVolume(
   var value : Int,
   var unit : String
)