package com.ni.punkapi.data.entities

data class Hops (
     var name : String,
     var amount : Amount,
    var add : String,
    var attribute : String

)
