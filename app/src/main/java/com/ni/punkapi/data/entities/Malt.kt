package com.ni.punkapi.data.entities

data class Malt(
    var name : String,
    var amount : Amount
)