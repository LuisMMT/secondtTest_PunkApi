package com.ni.punkapi.data.entities

data class Ingredients(
    var malt : List<Malt>,
    var hops : List<Hops>,
    var yeast : String
)