package com.ni.punkapi.data.remote

import javax.inject.Inject

class BeerRemoteDataSource @Inject constructor(
    private val beerService: BeerService
):BaseDataSource(){
    suspend fun getBeers() = getResult{ beerService.getAllBeers()}
    suspend fun getBeer(id:Int) = getResult{beerService.getBeer(id)}

}