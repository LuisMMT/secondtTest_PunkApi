package com.ni.punkapi.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Beers")
data class Beer(
        @PrimaryKey
        val id : Int,
        val name : String,
        val tagline : String,
        val first_brewed : String,
        val description : String,
        val image_url : String,
        val abv : Double,
        val ibu : Double,
        val target_fg : Int,
        val target_og : Double,
        val ebc : Int,
        val srm : Double,
        val ph : Double,
        val attenuation_level : Double,
    //    var volume : Volume,
       // var boilVolume : BoilVolume,
     //   var method : Method,
    //    var ingredients : Ingredients,
       // var foodPairing : List<String>,
        val brewers_tips : String,
        val contributed_by : String
)