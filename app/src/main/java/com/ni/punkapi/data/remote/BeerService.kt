package com.ni.punkapi.data.remote

import com.ni.punkapi.data.entities.Beer
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface BeerService {
    @GET("beers")
    suspend fun getAllBeers() : Response<List<Beer>>

    @GET("character/{id}")
    suspend fun getBeer(@Path("id") id: Int): Response<Beer>
}