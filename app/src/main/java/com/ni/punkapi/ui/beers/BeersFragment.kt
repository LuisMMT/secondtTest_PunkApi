package com.ni.punkapi.ui.beers

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ni.punkapi.R
import com.ni.punkapi.databinding.FragmentBeersBinding
import com.ni.punkapi.utils.Resource
import com.ni.punkapi.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class BeersFragment : Fragment(),BeersAdapter.BeerItemListener {

    private var binding : FragmentBeersBinding by autoCleared()
    private val viewModel: BeerViewModel by viewModels()
    private lateinit var adapter: BeersAdapter

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentBeersBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()


        /*view.findViewById<Button>(R.id.button_first).setOnClickListener {
            findNavController().navigate(R.id.action_BeersFragment_to_SecondFragment)
        }*/

    }

    private fun setupObservers() {
        viewModel.beers.observe(viewLifecycleOwner, Observer {
            when(it.status){
                Resource.Status.SUCCESS->{
                    binding.progressBar.visibility = View.GONE
                   if (!it.data.isNullOrEmpty())adapter.setItems((ArrayList(it.data)))
                }
                Resource.Status.ERROR->Toast.makeText(requireContext(),it.message,Toast.LENGTH_LONG).show()

                Resource.Status.LOADING-> binding.progressBar.visibility= View.VISIBLE
            }
        })
    }

    private fun setupRecyclerView() {
        adapter = BeersAdapter(this)
        binding.beersRv.layoutManager = LinearLayoutManager(requireContext())
        binding.beersRv.adapter = adapter
    }

    override fun onClickedBeer(beerId: Int) {

        findNavController().navigate(R.id.action_BeersFragment_to_SecondFragment)
        bundleOf("id" to beerId)
      /*  val sharedPref = activity?.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE)*/

       val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putInt(getString(R.string.preference_file_key), beerId)
            commit()
        }

    }
}