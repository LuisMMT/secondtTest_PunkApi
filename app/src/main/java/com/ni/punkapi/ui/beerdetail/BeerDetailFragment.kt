package com.ni.punkapi.ui.beerdetail

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ni.punkapi.R
import com.ni.punkapi.data.entities.Beer
import com.ni.punkapi.databinding.FragmentBeerDetailBinding
import com.ni.punkapi.utils.Resource
import com.ni.punkapi.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class BeerDetailFragment : Fragment() {

    private var binding: FragmentBeerDetailBinding by autoCleared()
    private val viewModel: BeerDetailViewModel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       // return inflater.inflate(R.layout.fragment_beer_detail, container, false)
        binding = FragmentBeerDetailBinding.inflate(inflater,container,false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_second).setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)}

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val defaultValue = resources.getInteger(R.integer.saved_high_score_default_key)
        val highScore = sharedPref.getInt(getString(R.string.preference_file_key), defaultValue)
        viewModel.start(highScore)
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.beer.observe(viewLifecycleOwner, Observer {
            when(it.status){
                Resource.Status.SUCCESS->{
                    binBeer(it.data!!)
                    binding.progressBar.visibility = View.GONE
                    binding.beerCl.visibility = View.VISIBLE
                }
                Resource.Status.ERROR->Toast.makeText(activity,it.message,Toast.LENGTH_LONG).show()

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.beerCl.visibility = View.GONE
                }
            }
        })
    }

    private fun binBeer(databeer: Beer) {
        binding.name.text = databeer.name
        binding.tagline.text = databeer.tagline
        binding.status.text = databeer.description
        binding.gender.text = databeer.first_brewed

        Glide.with(binding.root)
            .load(databeer.image_url)
            .transform(CircleCrop())
            .into(binding.image)
    }
}