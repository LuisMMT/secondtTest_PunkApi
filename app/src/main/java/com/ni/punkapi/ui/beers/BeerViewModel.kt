package com.ni.punkapi.ui.beers

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.ni.punkapi.data.repository.BeerRepository

class BeerViewModel @ViewModelInject constructor(
    private val repository: BeerRepository
): ViewModel(){
    val beers = repository.getBeers()
}