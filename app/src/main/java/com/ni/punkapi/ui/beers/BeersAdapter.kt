package com.ni.punkapi.ui.beers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ni.punkapi.data.entities.Beer
import com.ni.punkapi.databinding.ItemBeerBinding

class BeersAdapter(private val listener: BeerItemListener): RecyclerView.Adapter<BeerViewHolder>(){

    interface BeerItemListener{
        fun onClickedBeer(beerId: Int)
    }

    private val items = ArrayList<Beer>()

    fun setItems(items: ArrayList<Beer>){
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
      val binding: ItemBeerBinding = ItemBeerBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return BeerViewHolder(binding,listener)

    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) = holder.bind(items[position])
}
    class BeerViewHolder(private val itemBinding: ItemBeerBinding, private val listener: BeersAdapter.BeerItemListener): RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener{
        private lateinit var beer: Beer

        init {
            itemBinding.root.setOnClickListener(this)
        }

      fun bind(item:Beer){
          this.beer = item
          itemBinding.name.text = item.name
          itemBinding.tagline.text = item.tagline
          Glide.with(itemBinding.root)
              .load(item.image_url)
              .transform(CircleCrop())
              .into(itemBinding.image)
      }
        override fun onClick(v: View?) {
        listener.onClickedBeer(beer.id)
        }

    }
