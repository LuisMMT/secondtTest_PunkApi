package com.ni.punkapi.ui.beerdetail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.ni.punkapi.data.entities.Beer
import com.ni.punkapi.data.repository.BeerRepository
import com.ni.punkapi.utils.Resource

class BeerDetailViewModel @ViewModelInject constructor(
    private val repository: BeerRepository
): ViewModel(){
    private val _id = MutableLiveData<Int>()


    private val _beer = _id.switchMap { id ->
        repository.getBeer(id)
    }
    val beer: LiveData<Resource<Beer>> = _beer


    fun start(id: Int) {
        _id.value = id
    }
}